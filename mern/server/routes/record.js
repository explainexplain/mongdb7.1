const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const bodyParser = require('body-parser');
const ObjectId = require("mongodb").ObjectId;
let urlencodedParser = bodyParser.urlencoded({ extended: false });

recordRoutes.route("/products").get(function(req, res) {
    let db_connect = dbo.getDb("products");
    let filter = {};
    let sort = {};
    let limit = 0;
    if (req.query.filterBy && req.query.filterValue) {
        filter[req.query.filterBy] = req.query.filterValue;
    }
    if (req.query.sortBy) {
        sort = {[req.query.sortBy] : 1};
    }
    if (req.query.limit){
        limit = parseInt(req.query.limit);
    }
    db_connect.collection("products").find(filter).sort(sort).limit(limit).toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});

recordRoutes.route("/products").post(urlencodedParser,function(req, response){
    let db_connect = dbo.getDb("products");
    db_connect.collection("products").findOne({nazwa: `${req.body.nazwa}`}, function(err, result) {
        if (err) throw err;
        if(result){
            response.send("Taki produkt juz istnieje")
        }
        else {
            newProduct = {...req.body};
            db_connect.collection("products").insertOne(newProduct, function(err, res){
                if (err) throw err;
                response.send(res);
            });
        }
    });
    
});

recordRoutes.route("/products/:id").put(urlencodedParser,function(req, response){
    let db_connect = dbo.getDb("products");
    let myquery = {_id: ObjectId(req.params.id)};
    let newValues = {}
    if(req.body.nazwa){
        newValues.nazwa = req.body.nazwa
    }
    if(req.body.cena){
        newValues.cena = req.body.cena
    }
    if(req.body.opis){
        newValues.opis =  req.body.opis
    }
    if(req.body.ilosc){
        newValues.ilosc = req.body.ilosc
    }
    if(req.body.jednostka){
        newValues.jednostka = req.body.jednostka
    } 
    
    db_connect.collection("products").updateOne(myquery, { $set: newValues}, function(err, res){
        if (err) throw err;
        console.log("1 document updated successfully");
        response.json(res);
    });
});

recordRoutes.route("/products/:id").delete(function (req, res) {
    let db_connect = dbo.getDb("products");
    let myquery = {_id: ObjectId(req.params.id)};
    db_connect.collection("products").findOne(myquery, function(err, result) {
        if (err) throw err;
        if(result){
            db_connect.collection("products").deleteOne(myquery, function(err, obj){
                if (err) throw err;
                console.log("1 document deleted");
                res.json(obj);
            });
        }
        else {
            res.send("Nie ma takiego produtku");
        }
    });
})  

recordRoutes.route("/products/raport").get(function(req, res) {
    let db_connect = dbo.getDb("products");
    
    const pipeline = [
        {
            $addFields: {
                cena: { $toDouble: "$cena" },
                ilosc: { $toInt: "$ilosc" }
            }
        },
        {
            $project: {
                _id: 0,
                nazwa: 1,
                ilosc: 1,
                cena: 1,
                totalPrice: { $multiply: ["$ilosc", "$cena"] }
            }
        }
    ];
    db_connect.collection("products").aggregate(pipeline).toArray(function(err, result) {
        if (err) {
            console.log(err);
            res.send("Wystąpił błąd podczas generowania raportu.");
            return;
        }

        res.json(result);
    });
});

module.exports = recordRoutes;